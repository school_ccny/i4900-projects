/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

#include <openssl/hmac.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <gmp.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K){

	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */

	// Create Random bytes
	size_t Lnth = rsa_numBytesN(K);
	unsigned char* pTxt = malloc(Lnth);
	unsigned char* cTxt = malloc(Lnth);
	pTxt[Lnth-1] = 0;

	randBytes(pTxt,Lnth-1);

	// Encrypt the random bytes.
	size_t cTxt_len = rsa_encrypt(cTxt, pTxt, Lnth, K);
	unsigned char _hash[HASHLEN];

	// Create SHA256 
	SHA256(pTxt, Lnth, _hash);

	size_t _en_len = Lnth + HASHLEN;
	unsigned char* _encap = malloc(_en_len);
	memcpy(_encap,cTxt,Lnth);
	memcpy(_encap + Lnth,_hash,HASHLEN);

	FILE *f = fopen(fnOut, "w+");
	fwrite(_encap, _en_len, 1, f);
	fclose(f);

	// Create SKE_KEY 
	SKE_KEY ky;
	ske_keyGen(&ky,pTxt,Lnth);
	ske_encrypt_file(fnOut,fnIn,&ky,NULL,_en_len);

	free(pTxt); free(cTxt); free(_encap);

	return 0;
}

int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K){

	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */

	FILE *f = fopen(fnIn, "r");
	unsigned char c[HASHLEN + 1];
	c[HASHLEN] = 0;
	
	size_t m_len = rsa_numBytesN(K);
	unsigned char* encap = malloc(m_len + HASHLEN);
	
	fread(encap, m_len + HASHLEN, 1, f);
	fclose(f);

	unsigned char* RandEncrypt = malloc(m_len);
	memcpy(RandEncrypt, encap, m_len);
	
	unsigned char* RandDecrypt = malloc(m_len);
	rsa_decrypt(RandDecrypt,RandEncrypt,m_len,K);

	memcpy(c, encap + m_len, HASHLEN);

	unsigned char HASH[HASHLEN + 1];
	HASH[HASHLEN] = 0;

	// Create SHA256 
	SHA256(RandDecrypt, m_len, HASH);

	// Create SKE_KEY 
	SKE_KEY _K;
	ske_keyGen(&_K,RandDecrypt,m_len);
	ske_decrypt_file(fnOut,fnIn,&_K,m_len + HASHLEN);

	return 0;
}


int main(int argc, char *argv[]) {
	/* define long options */

	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;

	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */

	RSA_KEY K;
	rsa_initKey(&K);
	FILE* f_Public;
	FILE* f_Private;
	char* fPub = malloc(10);    
	char* fPrvt = malloc(10);   

	switch (mode) 
	{
		case ENC:
			
			f_Public = fopen(fnKey, "r");
			rsa_readPublic(f_Public, &K);
			fclose(f_Public);
			kem_encrypt(fnOut, fnIn, &K);

			break;

		case DEC:
			
			f_Private = fopen (fnKey, "r");
			rsa_readPrivate(f_Private, &K);
			fclose(f_Private);
			kem_decrypt(fnOut, fnIn, &K);

			break;

		case GEN:

			rsa_keyGen(nBits, &K);
			f_Private = fopen (fnOut, "w+");
			rsa_writePrivate(f_Private, &K);		
			fclose(f_Private);

			snprintf(fnIn, FNLEN, "%s.pub", fnOut);
		
			f_Public = fopen (fnIn, "w+");
			rsa_writePublic(f_Public, &K);
			fclose(f_Public);

			break;

		default:

			return 1;
	}

	rsa_shredKey(&K);
	

	return 0;
}
