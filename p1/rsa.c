#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}


void generateLargePrime( mpz_t bign, int keySize )
{
    NEWZ(tmp);
    unsigned char *rnd = malloc(keySize);

    while ( !mpz_cmp_ui( bign, 0) )
    {
		randBytes( rnd, keySize/2 );
		BYTES2Z( tmp, rnd, keySize/2 );
		if ( ISPRIME(tmp) )
			mpz_set( bign, tmp );
	}
    free(rnd);
}


int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);

    /* Create two prime numbers, p and q. Calculate n = p * q */
    generateLargePrime ( K->p, keyBits );
    generateLargePrime ( K->q, keyBits );
    mpz_mul ( K->n, K->p, K->q );

    // Find a number that's relatively prime to phi(n) = (p-1)*(q-1)
    NEWZ(p1); mpz_sub_ui ( p1, K->p, 1 );
    NEWZ(q1); mpz_sub_ui ( q1, K->q, 1 );
    NEWZ(phi); mpz_mul ( phi, p1, q1 );

    // Create e that is relatively prime to (p-1)*(q-1)
    NEWZ (theGcd);
    int keep_trying = 1;
    unsigned char *cnt = malloc(keyBits);

    while ( keep_trying == 1 )
    {
        randBytes ( cnt, keyBits );
        BYTES2Z( K->e, cnt, keyBits );
		mpz_gcd( theGcd, phi, K->e );
        if ( mpz_cmp_ui(theGcd, 1) == 0 )
        {
            keep_trying = 0;
            free(cnt);
        }
    }

    /*modular invert*/
    mpz_invert ( K->d, K->e, phi );

	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
    NEWZ(out);
    NEWZ(num);

    BYTES2Z(num, inBuf, len);

    /* encrypt */
    mpz_powm_sec (out, num, K->e, K->n);

    /* populate outbuf with encrypted content */
    Z2BYTES(outBuf, len, out);

    return len;
}

size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
    NEWZ(out);
    NEWZ(num);

    BYTES2Z(num, inBuf, len);

    /* encrypt */
    mpz_powm_sec (out, num, K->d, K->n);

    /* populate outbuf with encrypted content */
    Z2BYTES(outBuf, len, out);

    return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
