#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <openssl/err.h>

#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32 //HMAC length
#define IV_LEN 16 //Initialization vector length
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
 int i; //variable
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	 if(entropy == NULL){ //entropy is not given

	 	// one key for the mac, and one for AES
		//Generate HMAC key
	 	randBytes(K->hmacKey, KLEN_SKE);
	 	randBytes(K->aesKey, KLEN_SKE);
	 }
	 else{//Entropy Given. Apply KDF to entropy to get key

		unsigned char* keyBuffer;
		keyBuffer = malloc(KLEN_SKE*2);

		/* unsigned char *HMAC(const EVP_MD *evp_md, const void *key, int key_len, const unsigned char *d, int n, unsigned char *md, unsigned int *md_len);*/

		HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, keyBuffer, NULL);

		memcpy(K->hmacKey, keyBuffer, KLEN_SKE);
		memcpy(K->aesKey, keyBuffer + KLEN_SKE, KLEN_SKE);

		free(keyBuffer);
	 }
	return 0;
}

size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}

size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */

	if (!IV){ //No random IV is given
	 	IV = malloc(IV_LEN);
	 	FILE* frand = fopen("/dev/urandom", "rb");
		fread(IV,1,IV_LEN,frand);
		fclose(frand);
	 }

	 //printf("%s\n", "Ofure in ske_encrypt");

	 unsigned char encryptedText[len];
	 unsigned char encryptedTextLoc[len+IV_LEN];
	 unsigned char hmacBuffer[len];

	 memset(encryptedText,0,len);
	 memset(hmacBuffer,0,len);

	 //similar encyprtion implementation from ctr_example()

	//encrypt
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV)){
		ERR_print_errors_fp(stderr);
	}

	int nWritten;

	if (1 != EVP_EncryptUpdate(ctx, encryptedText, &nWritten, inBuf, len)){
		ERR_print_errors_fp(stderr);
	}
	//printf("Block size: %d", EVP_CIPHER_block_size(ctx));

	EVP_CIPHER_CTX_free(ctx); //free memory

	memcpy(outBuf, IV, IV_LEN); //copy IV into outputBuffer
	memcpy(outBuf+IV_LEN, encryptedText, nWritten);
	memcpy(encryptedTextLoc, IV, IV_LEN);
	memcpy(&encryptedTextLoc[IV_LEN], encryptedText, nWritten);

	//Create HMAC key that is nWritten+IV_LEN byte long
	HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, encryptedTextLoc, nWritten+IV_LEN, hmacBuffer, NULL);

	memcpy(outBuf+IV_LEN+nWritten, hmacBuffer, HM_LEN);

	size_t bytesWritten = IV_LEN + HM_LEN + nWritten;

	free(IV);

	return bytesWritten;
	/* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}


size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */

	int fileDescriptor = open(fnin, O_RDONLY); //Open input file for reading only.
	struct stat sb;
	char *fileMapping;
	int fileStat = fstat (fileDescriptor, &sb);
	//int fileSbSize = sb.st_size;

	//if (fileDescriptor < 0)return -1;
    //error opening fnin
    // printf("File Size: \t\t%d bytes\n",fileStat.st_size);
    //if (fileStat.st_size  < 1) return -1;
	// if(fstat(fileDescriptor, &fileStat) < 0)  return -1;

	fileMapping = mmap(0, sb.st_size, PROT_READ, MAP_PRIVATE, fileDescriptor, 0);
	//close(fileDescriptor);
	//if (fileMapping == MAP_FAILED)return -1;

	size_t encryptedTextLen = ske_getOutputLen (sb.st_size);
	unsigned char* encryptedText = malloc(encryptedTextLen);

	//now run ske_encrypt
	ske_encrypt(encryptedText,(unsigned char*)fileMapping,sb.st_size,K,IV);

	int fileOutput = open(fnout, O_RDWR | O_CREAT, 00666);

	lseek(fileOutput, offset_out, SEEK_SET);
	write(fileOutput, encryptedText, encryptedTextLen);
	fileStat = fstat (fileOutput, &sb);
	sb.st_size += encryptedTextLen;
	close(fileOutput);


	free(encryptedText);


	return 0;
}


size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	if (inBuf == NULL)
		return -1;

	unsigned char initVect[IV_LEN];
	unsigned char initVectLoc[len-HM_LEN];

	unsigned char hmacBuffer[HM_LEN + 1];
	memset(hmacBuffer,0,HM_LEN + 1);

	unsigned char hmacBufferLoc[HM_LEN + 1];
	memset(hmacBufferLoc,0,HM_LEN + 1);

	int encryptedTextLen = len - IV_LEN - HM_LEN;
	unsigned char encryptedText[len]; //encryptedText


	memcpy(initVect, inBuf, IV_LEN);
	memcpy(encryptedText, inBuf + IV_LEN, encryptedTextLen );
	memcpy(hmacBuffer, inBuf + IV_LEN + encryptedTextLen, HM_LEN);
	memcpy(initVectLoc, inBuf, len-HM_LEN);

	HMAC(EVP_sha256(),K->hmacKey,KLEN_SKE,initVectLoc,encryptedTextLen + IV_LEN,hmacBufferLoc,NULL);

	int nWritten = 0;

	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,initVect))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,encryptedText,encryptedTextLen))
		ERR_print_errors_fp(stderr);

	return nWritten;
}


size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
		/* TODO: write this. */
	int fileDescriptor =  open(fnin, O_RDONLY);
	int fileOutput;

	unsigned char *fileMapping;
	struct stat sb;
	int status = fstat (fileDescriptor, &sb);

	fileMapping = (unsigned char*) mmap(0, sb.st_size, PROT_READ, MAP_PRIVATE, fileDescriptor, 0);
	//close(fileDescriptor);

	size_t encryptedTextLen = sb.st_size - offset_in; //fileStat.st_size - 48;
	unsigned char* decryptedText = malloc(encryptedTextLen);
	size_t decryptedTextLen = ske_decrypt(decryptedText,fileMapping+ offset_in ,encryptedTextLen,K);

	fileOutput = open(fnout, O_WRONLY | O_CREAT |O_TRUNC, 00666);
	write(fileOutput, decryptedText, decryptedTextLen);

	close(fileOutput);
	free(decryptedText);

	return 0;
}
